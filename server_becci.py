#Web Server Python Alessandro Becci

import socketserver
import sys
import http.server
import signal

def kill_handler(signal, frame):
    print( 'Ctrl + C was pressed. The server is shutting down.')
    try:
      if( server ):
        server.server_close()
    finally:
      sys.exit(0)

#predispone un handler per gestire l'interrupt(Ctrl + C) proveniente da tastiera. 
signal.signal(signal.SIGINT, kill_handler)


# l'utente può scegliere il numero di porta attraverso linea di comando, altrimenti si userà un numero predefinito
if sys.argv[1:]:
  port = int(sys.argv[1])
else:
  port = 8080

#creo il server impostando come indirizzo localhost
server = socketserver.ThreadingTCPServer(('',port), http.server.SimpleHTTPRequestHandler )

#Assicura che da tastiera usando la combinazione
#di tasti Ctrl-C termini in modo pulito tutti i thread generati
server.daemon_threads = True  
#il Server acconsente al riutilizzo del socket anche se ancora non è stato
#rilasciato quello precedente, andandolo a sovrascrivere
server.allow_reuse_address = True  

# entra nel loop infinito, ne uscirà solo se premeremo ctrl+C
try:
  while True:
    print("The server is online ! Press ctrl + C when you want to kill it")
    sys.stdout.flush()
    server.serve_forever()
except KeyboardInterrupt:
  pass

server.server_close()